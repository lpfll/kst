Source: kst
Section: science
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: cmake (>=2.8),
               debhelper-compat (= 13),
               docbook-xsl,
               libcfitsio-dev,
               libgetdata-dev,
               libgsl-dev,
               libmatio-dev,
               libnetcdf-dev,
               pkg-config,
               qtbase5-dev,
               qtbase5-dev-tools,
               qttools5-dev,
               xsltproc
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://kst-plot.kde.org/
Vcs-Browser: https://salsa.debian.org/debian/kst
Vcs-Git: https://salsa.debian.org/debian/kst.git

Package: kst
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: kst-data (<< 2.0.6),
        libkst2core2 (<< 2.0.6),
        libkst2math2 (<< 2.0.6),
        libkst2widgets2 (<< 2.0.6)
Replaces: kst-data (<< 2.0.6),
          libkst2core2 (<< 2.0.6),
          libkst2math2 (<< 2.0.6),
          libkst2widgets2 (<< 2.0.6)
Recommends: kst-doc
Description: scientific data plotting tool
 Kst is a fast real-time large-dataset viewing and plotting tool.
 It has basic data analysis functionality, contains many powerful
 built-in features and is expandable with plugins and extensions.

Package: kst-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: set of tutorials for kst
 Kst is a fast real-time large-dataset viewing and plotting tool.
 It has basic data analysis functionality, contains many powerful
 built-in features and is expandable with plugins and extensions.
 .
 This package contains documentation files for kst.
